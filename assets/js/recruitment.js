(function ($) {
    $(document).scroll(function () {
        var scroll= $(document).scrollTop()
        var $el = $("#menu_desktop_recruit")
        if( scroll > 100 && !$el.hasClass( "has-sticky" ) ){
            $el.addClass("has-sticky" )
        }else if( scroll <= 100 && $el.hasClass( "has-sticky" ) ){
            $el.removeClass("has-sticky" )
        }
    })
})(jQuery);